using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount >= 2 && Input.touches[1].phase == TouchPhase.Began)
        {
            State.SimulateJVD = !State.SimulateJVD;
        }

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Stationary) {
            State.ShowPlanes = true;
		}
		else {
            State.ShowPlanes = false;
		}
    }
}
