using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTrigger : MonoBehaviour
{
    public Rigidbody rig;
	public bool isSimulated = false;

    // Start is called before the first frame update
    void Start()
    {
        //rig = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void Update()
	{
		if (State.SimulateJVD) { 
			// enable physics
			rig.detectCollisions = true;
			rig.useGravity = true;
			rig.isKinematic = false;
		}
		else {
			// disable physics
			rig.detectCollisions = false;
			rig.useGravity = false;
			rig.isKinematic = true;
		}
	}
}