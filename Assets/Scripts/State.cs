using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    // Toggle JVD physics and collision
    public static bool SimulateJVD = false;

    // Toggle visualization of detected planes
    public static bool ShowPlanes = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
